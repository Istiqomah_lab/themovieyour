# README #
Information project:
This Application can run in Xcode 10.1 with swift4.
Minimum requirement of this app is iOS 9.0.
For run the App, Please select simulator device with that minimum requirement of iOS and then press the Run button in Xcode
for test unit open TheMovieYoursTests and run the test.

There are nine main home page 
1. Top Rated Movies
2. Upcoming Movies
3. NowPlayingMovies
4. Popular Movies
5. Top Rated TVShows
6. Popular TVShows
7. On The Air TVShows
8. Airing Today TVShows
9. Popular People

from 9 main home, user can select one of the content lists:
this page call content controller, the information will provide is:
Movie:
1. Original Title
2. Release Data
3. Original Language
4. Genres
5. Rating
6. Synopsis
7. There is Add/Delete to Watch List
TVShow:
1. Original Name
2. First Air Date
3. Original Language
4. Genres
5. Rating
6. Overview
7. There is Add/Delete to Watch List

Popular Person:
1. Name
2. Place of Birth
3. Birthday
4. Biography
5.  List Movie or TV show of that person, and the user can select the content of that

There are additional pages:
1. WatchList Page, user can select the content of that and delete as a watch list
2. Search page, user can select the content of that and can add as a watch list.






