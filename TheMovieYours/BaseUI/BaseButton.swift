//
//  BaseButton.swift
//  TheMovieYours
//
//  Created by User on 2019/7/1.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class BaseButton: UIButton{
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.updateUI()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.updateUI()
    }
    
    //MARK: button just with corner radius and set font size
    func updateUI() {
        self.layer.cornerRadius = 4;
        self.clipsToBounds = true;
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        self.backgroundColor = UIColor.init(hex: color_base_white).withAlphaComponent(0.9)
        self.setTitleColor(UIColor.init(hex:background_color ).withAlphaComponent(0.75), for: .normal)
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true;
        
    }
    
    func updateButtonWithBorder() {
        self.backgroundColor = UIColor.clear
        self.setTitleColor(UIColor.init(hex:background_color ).withAlphaComponent(0.9), for: .normal)
        self.layer.borderColor = UIColor.init(hex: background_color).withAlphaComponent(0.5).cgColor
        self.layer.borderWidth = 1;
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true;
    }
    
    func updateButtonWithImageAndBorder(imageName: String) {
        self.backgroundColor = UIColor.init(hex: background_color)
        self.setImage(UIImage.init(named: imageName), for: .normal)
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true;
    }
    func updateButtonWithImage(imageName: String) {
        self.backgroundColor = UIColor.init(hex: background_color)
        self.setImage(UIImage.init(named: imageName), for: .normal)
        //        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true;
    }
    func updateButtonWithBackgroundColor() {
        self.backgroundColor = UIColor.init(hex: background_color)
        self.setTitleColor(UIColor.init(hex:color_base_white ),for: .normal)
    }
    
    
}

