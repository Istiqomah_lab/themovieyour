//
//  BaseLabel.swift
//  TheMovieYours
//
//  Created by User on 2019/6/28.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        defultLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func defultLabel() {
        self.font = UIFont.systemFont(ofSize: 15)
        self.textColor = UIColor.init(hex: color_text_default)
    }
    
    func titleDefultLabel() {
        self.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    // MARK: - TypeLabel

}
