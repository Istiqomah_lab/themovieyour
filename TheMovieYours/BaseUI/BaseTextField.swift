//
//  BaseTextField.swift
//  TheMovieYours
//
//  Created by User on 2019/7/3.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class BaseTextField: UITextField {
    let inset: CGFloat = 10
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.updateUI()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.updateUI()
    }
    
    //MARK: setting all type textfield
    func updateUI() {
        
        let fontTextField = UIFont.systemFont(ofSize: 16)
        self.font = fontTextField
        self.backgroundColor = UIColor.clear
        self.textAlignment = .left
        
        self.layer.borderColor = UIColor.init(hex: color_base_white).withAlphaComponent(0.5).cgColor
        self.layer.borderWidth = 1;
        self.layer.masksToBounds = true;
        
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true;
        self.attributedPlaceholder = NSAttributedString(string: "placeholder text",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hex: color_base_white).withAlphaComponent(0.75)])
        
        self.textColor = UIColor.init(hex: color_base_white)
        
    }
    func updateDifferentClolor() {
        
        let fontTextField = UIFont.systemFont(ofSize: 16)
        self.font = fontTextField
        self.backgroundColor = UIColor.clear
        self.textAlignment = .left
        
        self.layer.borderColor = UIColor.init(hex: background_color).withAlphaComponent(0.5).cgColor
        self.layer.borderWidth = 1;
        self.layer.masksToBounds = true;
        
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true;
        self.attributedPlaceholder = NSAttributedString(string: "placeholder text",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hex: background_color).withAlphaComponent(0.75)])
        
        self.textColor = UIColor.init(hex: background_color)
        
    }
    //MARK: add left gap for textfield
    func addInset() {
        self.leftViewMode = UITextField.ViewMode.always
        let container = UIView()
        container.frame = CGRect(x: 0, y: 0, width: 10, height: self.bounds.size.height)
        self.leftView = container
    }
    func addInsetImage(imageName: String) {
        self.leftViewMode = UITextField.ViewMode.always
        let container = UIView()
        container.frame = CGRect(x: 10, y: 10, width: 35, height: 35)
        let image = UIImageView.init(image: UIImage.init(named: imageName))
        image.frame = CGRect(x: 5, y: 0, width: 30, height: 30)
        image.contentMode = .scaleAspectFit
        
        container.addSubview(image)
        self.leftView = container
    }
    func updatePlaceholder(String:String) {
        self.attributedPlaceholder = NSAttributedString(string: String,
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hex:color_btn_gray).withAlphaComponent(0.75)])
    }
    
}

