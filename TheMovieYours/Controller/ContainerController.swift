//
//  ContainerController.swift
//  TheMovieYours
//
//  Created by User on 2019/6/27.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

private let reuseIdentifier = "reuseContentCell"
class ContainerController: UIViewController {
    
    // MARK: - Properties
    var homeController: HomeController!
    var menuController: MenuController!
    var centralController:UIViewController!
    var isExpand: Bool = false
    let titleLabel = BaseLabel()
    var movieList: [Movie]  = []
    var TVShowList: [TVShow]  = []
    var popularPersonList: [PopularPerson]  = []
    var personBio: PersonProfile!
    var typePage: Int = 0
    var localStorage = LocalStorage()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isScrollEnabled =  true
        collectionView.backgroundColor = UIColor.init(hex: background_color).withAlphaComponent(0.5)
        return collectionView
    }()
    
    let indicatorView: UIView = {
       let viewbackground = UIView()
        viewbackground.backgroundColor = UIColor.init(hex: color_base_black).withAlphaComponent(0.5)
        viewbackground.translatesAutoresizingMaskIntoConstraints = false
        
        return viewbackground
    }()
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        ConfigureHomeController()
        configureTitle()
        configureContentView()
        configureIndicatorView()
        if self.currentReachabilityStatus == .notReachable {
            self.alertConnection()
        } else {
             getData(menuList: MenuList(rawValue: 0)!)
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    // MARK: - Handlers
    func ConfigureHomeController() {
        homeController = HomeController()
        homeController.titleApp = "The Movie Your"
        homeController.delegate = self
        centralController = UINavigationController(rootViewController: homeController)
        view.addSubview(centralController.view)
        addChild(centralController)
        centralController.didMove(toParent: self)
        
    }
    
    func ConfigureMenuController() {
        if menuController == nil{
            menuController = MenuController()
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
            print("move menu side")
            
        }
    }
    
    func configureTitle() {
        titleLabel.titleDefultLabel()
        titleLabel.text = MenuList(rawValue: 0)?.description
        homeController.view.insertSubview(titleLabel, at: 0)
        titleLabel.translatesAutoresizingMaskIntoConstraints =  false
        titleLabel.leftAnchor.constraint(equalTo: homeController.view.leftAnchor, constant: 20).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: homeController.view.rightAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        titleLabel.topAnchor.constraint(equalTo: homeController.view.topAnchor, constant: (homeController.navigationController?.navigationBar.frame.height ?? 80) + 10).isActive = true
        
        let searchBtn = BaseButton()
        searchBtn.setTitle(ListBtn.Search.description, for: .normal)
        searchBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        searchBtn.addTarget(self, action: #selector(actionSearch), for: .touchUpInside)
        searchBtn.updateButtonWithBorder()
        homeController.view.insertSubview(searchBtn, at: 1)
        searchBtn.translatesAutoresizingMaskIntoConstraints =  false
        searchBtn.rightAnchor.constraint(equalTo: homeController.view.rightAnchor, constant: -10).isActive = true
        searchBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        searchBtn.topAnchor.constraint(equalTo: homeController.view.topAnchor, constant: (homeController.navigationController?.navigationBar.frame.height ?? 80) + 25).isActive = true
        
    }
    func configureContentView()  {
        collectionView.dataSource = self as? UICollectionViewDataSource
        collectionView.delegate = self as? UICollectionViewDelegate
        collectionView.register(ContentCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        homeController.view.addSubview(collectionView)
        collectionView.bottomAnchor.constraint(equalTo: homeController.view.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: homeController.view.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: homeController.view.leftAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: homeController.view.topAnchor, constant: (homeController.navigationController?.navigationBar.frame.height ?? 80) + 60).isActive = true
        
    }
    func configureIndicatorView(){
        let viewIndicator = UIView(frame: CGRect(x: (homeController.view.frame.width / 2) - 50, y: (homeController.view.frame.height / 2) - 50, width: 100, height: 100))
        viewIndicator.layer.cornerRadius = 8;
        viewIndicator.clipsToBounds = true;
        viewIndicator.backgroundColor = UIColor.init(hex: color_base_white)
        viewIndicator.translatesAutoresizingMaskIntoConstraints = false
        let activityView = UIActivityIndicatorView(frame: CGRect(x:0, y: 0, width: 100, height: 70))
        activityView.style = .gray
        activityView.startAnimating()
        viewIndicator.addSubview(activityView)
        let labelIndicator = BaseLabel(frame: CGRect(x:0, y: 70, width: 100, height: 30))
        labelIndicator.textColor = .gray
        labelIndicator.titleDefultLabel()
        labelIndicator.textAlignment = .center
        labelIndicator.text = "Loading"
        viewIndicator.addSubview(labelIndicator)
        indicatorView.insertSubview(viewIndicator, at: 0)
        
        homeController.view.insertSubview(indicatorView, aboveSubview: collectionView)
        indicatorView.bottomAnchor.constraint(equalTo: homeController.view.bottomAnchor).isActive = true
        indicatorView.rightAnchor.constraint(equalTo: homeController.view.rightAnchor).isActive = true
        indicatorView.leftAnchor.constraint(equalTo: homeController.view.leftAnchor).isActive = true
        indicatorView.topAnchor.constraint(equalTo: homeController.view.topAnchor, constant: (homeController.navigationController?.navigationBar.frame.height ?? 80) + 60).isActive = true
    }
    
    @objc func actionSearch(){
        let searchController = SearchController()
        present(UINavigationController(rootViewController: searchController), animated: true, completion: nil)
    }
    func showMenuSlide(shouldExpand:Bool, menuOption:MenuList?) {
        if shouldExpand{
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centralController.view.frame.origin.x = self.centralController.view.frame.width - 80
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centralController.view.frame.origin.x = 0
                guard let menuList = menuOption else {return}
                self.didSelectMenuList(menuList: menuList)
            }) { (_) in
                
            }
        }
    }
    func didSelectMenuList(menuList: MenuList) {
        if self.currentReachabilityStatus == .notReachable {
            self.alertConnection()
        } else {
             getData(menuList: menuList)
        }
        self.typePage = menuList.typeContent
        self.titleLabel.text = menuList.description
        switch menuList {
        case .TopRatedMovies:
            print("Top Rate Movie")
        case .UpcomingMovies:
            print("Upcoming Movies")
        case .NowPlayingMovies:
            print("Now Playing Movies")
        case .PopularMovies:
            print("Popular Movies")
        case .TopRatedTVShows:
            print("Top Rated TV Shows")
        case .PopularTVShows:
            print("Popular TV Shows")
        case .OnTheAirTVShows:
            print("On The Air TV Shows")
        case .AiringTodayTVShows:
            print("Airing Today TV Shows")
        case .PopularPeople:
            print("Popular People")
        }
    }
    func getData(menuList: MenuList) {
        print("get data")
        indicatorView.isHidden = false
        if menuList.typeContent == 0{
            self.movieList.removeAll()
            self.TVShowList.removeAll()
            self.popularPersonList.removeAll()
            updateContent()
            APIManager.init().getMovieContent(typeAPI: (menuList.APIDescription), page: "1") { (result, err) in
                if !err {
                    print("get data")
                    self.movieList = result
//                    for list in self.movieList {
//                        print(list.vote_average)
//                    }
                   
                    self.updateContent()
                    DispatchQueue.main.async {
                        self.indicatorView.isHidden = true
                    }
                    
                    
                    
                }
            }
        } else if menuList.typeContent == 1{
            self.movieList.removeAll()
            self.TVShowList.removeAll()
            self.popularPersonList.removeAll()
            updateContent()
            APIManager.init().getTVShowContent(typeAPI: (menuList.APIDescription), page: "1") { (result, err) in
                if !err {
                    
                    self.TVShowList = result
//                    for list in self.TVShowList {
//                        print(list.vote_average)
//                    }
                    self.updateContent()
                     DispatchQueue.main.async {
                         self.indicatorView.isHidden = true
                     }
                   
                    
                    
                }
            }
        } else  if menuList.typeContent == 2{
            self.movieList.removeAll()
            self.TVShowList.removeAll()
            self.popularPersonList.removeAll()
            updateContent()
            APIManager.init().getPopularPersonContent(typeAPI: (menuList.APIDescription), page: "1") { (result, err) in
                if !err {
                    
                    self.popularPersonList = result
//                    for list in self.popularPersonList {
//                    }
                       self.updateContent()
                    DispatchQueue.main.async {
                        self.indicatorView.isHidden = true
                    }
                }
            }
        }
        
    }
    
    func updateContent(){
        self.collectionView.reloadData()
        let context = self.collectionView.collectionViewLayout.invalidationContext(forBoundsChange: self.collectionView.bounds)
        context.contentOffsetAdjustment = CGPoint.zero
        self.collectionView.collectionViewLayout.invalidateLayout(with: context)
        self.collectionView.layoutSubviews()
    }
    func alertConnection(){
        let alert = UIAlertController(title: "Connection", message: "There isn't internet connection", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(5.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            alert.dismiss(animated: true, completion: {() -> Void in
               
            })
            
        })
    }
}

extension ContainerController:HomeControllerDelegate{
    
    func handleWatchList() {
        print("watch list")
        let watchListController = WatchListController()
        present(UINavigationController(rootViewController: watchListController), animated: true, completion: nil)
    }
    
  
    func handleMenuToggle(forMenuOption menuOption: MenuList?) {
        if !isExpand {
            ConfigureMenuController()
        }
        isExpand = !isExpand
        showMenuSlide(shouldExpand: isExpand, menuOption: menuOption)
        
    }
    
}
extension ContainerController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 10
        
        if self.typePage == 0{
            count = movieList.count
        } else if self.typePage == 1{
            count = TVShowList.count
        } else {
            count = popularPersonList.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ContentCell
        cell.backgroundColor = .white
        cell.layer.cornerRadius = 4;
        cell.clipsToBounds = true;
        
        if self.typePage == 0{
            cell.titleLabel.text = self.movieList[indexPath.row].title as String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.movieList[indexPath.row].poster_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        } else if self.typePage == 1{
            cell.titleLabel.text = self.TVShowList[indexPath.row].title as String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.TVShowList[indexPath.row].poster_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        } else if self.typePage == 2 {
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.popularPersonList[indexPath.row].profile_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
            cell.titleLabel.text = self.popularPersonList[indexPath.row].title as String
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell: CGFloat = (self.view.frame.width - 40) / 2
        return CGSize(width: widthCell, height: widthCell * 1.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print(indexPath.row)
        let contentController = ContentController()
        contentController.typePage = self.typePage
        if self.typePage == 0 {
            contentController.movieSelected = self.movieList[indexPath.row]
            print(self.movieList[indexPath.row].id)
            let isAxist : Bool = localStorage.isDataAxist(id: self.movieList[indexPath.row].id, contentType: self.typePage) as Bool
            contentController.isWatchList = isAxist
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
        } else if self.typePage == 1 {
           contentController.TVShowSelected =  self.TVShowList[indexPath.row]
            let isAxist : Bool = localStorage.isDataAxist(id: self.TVShowList[indexPath.row].id, contentType: self.typePage) as Bool
            contentController.isWatchList = isAxist
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
            
        } else  if self.typePage == 2 {
            if self.currentReachabilityStatus == .notReachable {
                self.alertConnection()
            } else {
                self.indicatorView.isHidden = false
                APIManager.init().getPersonBiography(id: self.popularPersonList[indexPath.row].id as Int) { (result, err) in
                    if !err {
                        self.personBio = result
                        self.indicatorView.isHidden = true
                        let popularController = PopularController()
                        popularController.popularPersonSelected = self.popularPersonList[indexPath.row]
                        popularController.personBio = self.personBio
                        self.present(UINavigationController(rootViewController: popularController), animated: true, completion: nil)
                    }
                }
            }
            
            
        }
        
        
        
    }
    
    
}


