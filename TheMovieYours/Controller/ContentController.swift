//
//  ContentController.swift
//  TheMovieYours
//
//  Created by User on 2019/7/1.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class ContentController: UIViewController {
    
    // MARK: - Properties
    var movieSelected: Movie!
    var TVShowSelected: TVShow!
    var popularPersonSelected:PopularPerson!
    var typePage: Int!
    var arrayMenu: NSArray!
    var isWatchList: Bool = false
    var watchListBtn : BaseButton!
    //init realM
    var localStorage = LocalStorage()
    
    
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
    
        view.backgroundColor = .white
        convigureNavigationBar()
        
        if self.typePage != 2  {
            print("from content \(self.typePage == 0 ? self.movieSelected.original_title: self.TVShowSelected.original_name)")
            arrayMenu = [[
            
                "title" : "\(self.typePage == 0 ? ListLableMovie(rawValue: 0)!.description :ListLableTVShow(rawValue: 0)!.description )",
                    "Content" :"\(self.typePage == 0 ? self.movieSelected.original_title: self.TVShowSelected.original_name )"
                ],[
                
                    "title" : "\(self.typePage == 0 ? ListLableMovie(rawValue: 1)!.description :ListLableTVShow(rawValue: 1)!.description)",
                    "Content" :"\(self.typePage == 0 ? self.movieSelected.release_date: self.TVShowSelected.first_air_date )"
                ],[
                    
                    "title" : "\(self.typePage == 0 ? ListLableMovie(rawValue: 2)!.description :ListLableTVShow(rawValue: 2)!.description)",
                    "Content" :"\(self.typePage == 0 ? self.movieSelected.original_language: self.TVShowSelected.original_language )"
                ],[
                    
                    "title" : "\(self.typePage == 0 ? ListLableMovie(rawValue: 3)!.description:ListLableTVShow(rawValue: 3)!.description)",
                    "Content" :"\(self.typePage == 0 ? self.movieSelected.genre_ids: self.TVShowSelected.genre_ids )"
                ],[
                    
                    "title" : "\(self.typePage == 0 ? ListLableMovie(rawValue: 4)!.description :ListLableTVShow(rawValue: 4)!.description)",
                    "Content" :"\(self.typePage == 0 ? self.movieSelected.vote_average: self.TVShowSelected.vote_average )"
                ],[
                    
                    "title" : "\(self.typePage == 0 ? ListLableMovie(rawValue: 5)!.description:ListLableTVShow(rawValue: 5)!.description)",
                    "Content" :"\(self.typePage == 0 ? self.movieSelected.overview: self.TVShowSelected.overview )"
                ]]
            
            configureFirstContent()
        } else  if self.typePage == 2 {
             print("from content \(popularPersonSelected.title)")
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Hendlers
    func convigureNavigationBar(){
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: background_color)
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "\(self.typePage == 0 ? self.movieSelected.title:self.typePage == 1 ? self.TVShowSelected.title : self.popularPersonSelected.title)"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "baseline_clear_white_36pt_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handlesDismiss))
    }
    
    @objc func handlesDismiss () {
        dismiss(animated: true, completion: nil)
    }

    func configureFirstContent(){
        var textView = UITextView()
        let widthCell: CGFloat = (self.view.frame.width - 40) / 2
        let imagePoster = UIImageView(frame: CGRect(x: 10, y: (navigationController?.navigationBar.frame.height ?? 80) + 30, width: widthCell, height: widthCell * 1.5))
        imagePoster.backgroundColor = .gray
        let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.typePage == 0 ? self.movieSelected.poster_path:self.typePage == 1 ? self.TVShowSelected.poster_path : self.popularPersonSelected.profile_path)")
        Alamofire.request(url!).responseData { response in
            if let image = response.result.value {
                imagePoster.image = UIImage(data: image)
                
            }
        }
        view.addSubview(imagePoster)
        var y: CGFloat = (navigationController?.navigationBar.frame.height ?? 80) + 30
        let weightLabel : CGFloat = (self.view.frame.width - 40) / 2
        let x : CGFloat = 20 + ((self.view.frame.width - 40) / 2)
        for index in 0..<arrayMenu.count{
            var ytView: CGFloat = imagePoster.frame.origin.y + (widthCell * 1.5)
            if y > ytView {
                ytView = y + 20
            }
            var labelTitle = BaseLabel(frame: CGRect(x: index == 5 ? 10: x, y: index == 5 ? ytView : y, width: index == 5 ? weightLabel * 2: index == 4 ? weightLabel * 0.5 : weightLabel, height: 30))
            labelTitle.textColor = UIColor.init(hex: background_color)
            labelTitle.font = UIFont.boldSystemFont(ofSize: 15)
            labelTitle.text = "\((arrayMenu[index] as! NSDictionary)["title"] as! String)  :"
            view.addSubview(labelTitle)
            y += 20
            if index == 3 {
                var arrayTemporary: [Int]!
                if self.typePage == 0 {
                    arrayTemporary = self.movieSelected.genre_ids as NSArray as? [Int]
                } else if self.typePage == 1 {
                    arrayTemporary = self.TVShowSelected.genre_ids as NSArray as? [Int]
                }
                var index : Int = 0
                for list in (arrayTemporary) {
                    if index <= 3 {
                        var genre = ""
                        if self.typePage == 0 {
                            genre = (genresMovie(rawValue: list)?.description)!
                        } else if self.typePage == 1 {
                            genre = (genresTVShow(rawValue: list)?.description)!
                        }
                        labelTitle = BaseLabel(frame: CGRect(x:  x + 10 , y: y, width: index == 4 ? weightLabel * 0.5 : weightLabel, height: 30))
                        labelTitle.font = UIFont.systemFont(ofSize: 15)
                        labelTitle.text = genre
                        view.addSubview(labelTitle)
                        y += 20
                        index += 1
                    }
                    
                    
                }
            }else if (index == 5) {
                textView = UITextView(frame: CGRect(x: 10, y:ytView + 25, width:  (weightLabel * 2) + 20, height: self.view.frame.height - y - 70 ))
                textView.isEditable = false
                textView.textColor = UIColor.init(hex: background_color)
                textView.textAlignment = .justified
                textView.font = UIFont.systemFont(ofSize: 14)
                textView.textContainer.maximumNumberOfLines = 15
                textView.textContainer.lineBreakMode = .byTruncatingTail
                textView.text = (arrayMenu[index] as! NSDictionary)["Content"] as? String
                view.addSubview(textView)
            } else {
                labelTitle = BaseLabel(frame: CGRect(x: index == 4 ? x + 20 + (weightLabel * 0.5) : x + 20, y: index == 4 ? y - 20:y, width: index == 4 ? (weightLabel * 2) + 20: weightLabel, height:  30))
                labelTitle.font = UIFont.systemFont(ofSize: 15)
                labelTitle.text = index == 4 ? "\((arrayMenu[index] as! NSDictionary)["Content"] as! String)/10":(arrayMenu[index] as! NSDictionary)["Content"] as? String
                view.addSubview(labelTitle)
                y += 20
                if index == 4 {
                    y -= 40
                }
                
            }
            
        }
        
        
    //button
         watchListBtn = BaseButton(frame: CGRect(x: 10, y: self.view.frame.height - 80, width: self.view.frame.width - 20, height: 70))
        watchListBtn.updateButtonWithBackgroundColor()
        watchListBtn.setTitle("\(isWatchList == false ? "Add Watch  List": "Delete Watch  List")", for: .normal)
        watchListBtn.addTarget(self, action: #selector(addWatchList), for: .touchUpInside)
        view.addSubview(watchListBtn)
        
    }
    
     @objc func addWatchList() {
        if isWatchList == true {
            watchListBtn.setTitle("Add Watch  List", for: .normal)
            localStorage.delateDataById(id: self.typePage == 0 ? movieSelected.id:TVShowSelected.id, contentType: self.typePage)
        } else if isWatchList == false {
            watchListBtn.setTitle("Delete Watch  List", for: .normal)
            if self.typePage == 0 {
                localStorage.saveMovieWatchList(movie: movieSelected)
            } else if self.typePage == 1 {
                localStorage.saveTVShowWatchList(tvShow: TVShowSelected)
            }
        }
        isWatchList = !isWatchList
        
        
        
    }
    
}
