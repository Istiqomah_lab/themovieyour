//
//  HomeController.swift
//  TheMovieYours
//
//  Created by User on 2019/6/27.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class HomeController: UIViewController{
    
    // MARK: - Properties
    var titleApp: String!
    var delegate: HomeControllerDelegate?
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        ConfigureNavigationBar()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    @objc func handleMenuToggle(){
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    @objc func handleWatchList(){
        delegate?.handleWatchList()
    }
    
    // MARK: - Handlers
    func ConfigureNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: background_color)
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.title = titleApp
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: ListBtn.WatchList.description, style: .plain, target: self, action: #selector(handleWatchList))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.init(hex: color_base_white)
        navigationItem.rightBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 10, vertical: 10), for: UIBarMetrics.default)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hex: color_base_white),NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)], for: .selected)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hex: color_base_white),NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)], for: .normal)
    }
    
}


