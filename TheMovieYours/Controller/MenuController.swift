//
//  MenuController.swift
//  TheMovieYours
//
//  Created by User on 2019/6/27.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

private let reuseIdentifier = "MenuListCell"
class MenuController: UIViewController{
    
    // MARK: - Properties
    var tableView: UITableView!
    var delegate: HomeControllerDelegate?
    
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureTableView()
    }
    
    // MARK: - Handlers
    func configureTableView() {
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MenuListCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.backgroundColor = UIColor.init(hex: background_color)
        tableView.separatorStyle = .none
        tableView.rowHeight = 50
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

extension MenuController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MenuListCell
        let menuList = MenuList(rawValue: indexPath.row)
        cell.descriotionLabel.text = menuList?.description
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuList = MenuList(rawValue: indexPath.row)
        delegate?.handleMenuToggle(forMenuOption: menuList)
        

    }
    
}
