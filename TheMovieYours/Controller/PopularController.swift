//
//  PopularController.swift
//  TheMovieYours
//
//  Created by User on 2019/7/1.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
class PopularController: UIViewController {

    // MARK: - Properties
    var popularPersonSelected:PopularPerson!
    var personBio: PersonProfile!
    var arrayMenu: NSArray!
    var arrayContent: NSArray!
    let cellId = "cellId"
    //init realM
    var localStorage = LocalStorage()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isScrollEnabled =  true
        collectionView.backgroundColor = UIColor.init(hex: background_color).withAlphaComponent(0.5)
        return collectionView
    }()
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        arrayMenu = [[
                "title" : ListLablePerson(rawValue: 0)!.description,
                "Content" :self.personBio.also_known_as
            ],[
                "title" : ListLablePerson(rawValue: 1)!.description,
                "Content" :"\(self.personBio.place_of_birth)"
            ],[
                "title" : ListLablePerson(rawValue: 2)!.description,
                "Content" :"\(self.personBio.birthday)"
            ],[
                "title" : ListLablePerson(rawValue: 3)!.description,
                "Content" :"\(self.personBio.biography)"
            ]]
        configureNavigationBar()
        configuraContent()
        
        self.arrayContent = self.popularPersonSelected.known_for as NSArray
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func configureNavigationBar(){
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: background_color)
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = popularPersonSelected.title
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "baseline_clear_white_36pt_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handlesDismiss))
    }
    
    @objc func handlesDismiss () {
        dismiss(animated: true, completion: nil)
    }

    func configuraContent (){
         var textView = UITextView()
        let widthCell: CGFloat = (self.view.frame.width - 40) / 2
        let imagePoster = UIImageView(frame: CGRect(x: 10, y: (navigationController?.navigationBar.frame.height ?? 80) + 30, width: widthCell, height: widthCell * 1.5))
        imagePoster.backgroundColor = .gray
        let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.popularPersonSelected.profile_path)")
        Alamofire.request(url!).responseData { response in
            if let image = response.result.value {
                imagePoster.image = UIImage(data: image)
            }
        }
        view.addSubview(imagePoster)
        var y: CGFloat = (navigationController?.navigationBar.frame.height ?? 80) + 30
        let weightLabel : CGFloat = (self.view.frame.width - 40) / 2
        let x : CGFloat = 20 + ((self.view.frame.width - 40) / 2)
        var ytView: CGFloat = imagePoster.frame.origin.y + (widthCell * 1.5)
        for index in 0..<arrayMenu.count{
            if y > ytView {
                ytView = y + 20
            }
            var labelTitle = BaseLabel(frame: CGRect(x: index == 3 ? 10: x, y: index == 3 ? ytView : y, width: weightLabel, height: 30))
            labelTitle.textColor = UIColor.init(hex: background_color)
            labelTitle.font = UIFont.boldSystemFont(ofSize: 15)
            labelTitle.text = "\((arrayMenu[index] as! NSDictionary)["title"] as! String)  :"
            view.addSubview(labelTitle)
            y += 20
            if index == 0 {
                let array : NSArray = (arrayMenu[index] as! NSDictionary)["Content"] as! NSArray
                var index : Int = 0
                for list in array {
                    if index <= 4{
                    labelTitle = BaseLabel(frame: CGRect(x: x + 10 , y: y, width: index == 4 ? weightLabel * 0.5 : weightLabel, height: 30))
                    labelTitle.font = UIFont.systemFont(ofSize: 15)
                    labelTitle.text = list as? String
                    view.addSubview(labelTitle)
                    y += 20
                    index += 1
                    }
                }
            } else if index >= 1 && index != 3{
                labelTitle = BaseLabel(frame: CGRect(x:  x + 10 , y: y, width: index == 4 ? weightLabel * 0.5 : weightLabel, height: 30))
                labelTitle.font = UIFont.systemFont(ofSize: 15)
                labelTitle.text = "\((arrayMenu[index] as! NSDictionary)["Content"] as! String)"
                view.addSubview(labelTitle)
                y += 20
            } else if  index == 3{
                textView = UITextView(frame: CGRect(x: 10, y:ytView + 25, width:  (weightLabel * 2) + 20, height: 62 ))
                textView.isEditable = false
                textView.textColor = UIColor.init(hex: background_color)
                textView.textAlignment = .justified
                textView.font = UIFont.systemFont(ofSize: 14)
                textView.textContainer.maximumNumberOfLines = 15
                textView.textContainer.lineBreakMode = .byTruncatingTail
                textView.text = (arrayMenu[index] as! NSDictionary)["Content"] as? String
                view.addSubview(textView)
                ytView += 102
            }
        }
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(ContentCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: ytView).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        
    }
    
    
    
}
extension PopularController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ContentCell
        cell.backgroundColor = .white
        cell.layer.cornerRadius = 4;
        cell.clipsToBounds = true;
        let contentType = (self.arrayContent[indexPath.row] as! NSDictionary)["media_type"] as! String
        print(contentType)
        let title: String = contentType == "movie" ? ((self.arrayContent[indexPath.row] as! NSDictionary)["title"] as! String) :  ((self.arrayContent[indexPath.row] as! NSDictionary)["name"] as! String)
        cell.titleLabel.text = title
        
        
        if ((self.arrayContent[indexPath.row] as! NSDictionary)["poster_path"] as Any) is String{
            let posterPath: String = (self.arrayContent[indexPath.row] as! NSDictionary)["poster_path"] as! String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(posterPath)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        }
        
        print(self.arrayContent[indexPath.row] as! NSDictionary)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Height: CGFloat = (self.collectionView.frame.height) - 20
        return CGSize(width: Height * 0.75, height: Height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let dataSelected = self.arrayContent[indexPath.row] as! NSDictionary
        let contentType: Int = dataSelected["media_type"] as! String == "movie" ? 0:1
        let contentController = ContentController()
         contentController.typePage = contentType
        if contentType == 0 {
            contentController.movieSelected = Movie(id: dataSelected["id"] as! Int, vote_average: dataSelected["vote_average"] as! Double, genre_ids: dataSelected["genre_ids"] as! [Int], original_title: dataSelected["original_title"] as! String, backdrop_path: dataSelected["backdrop_path"] as! String, adult:  dataSelected["adult"] as! Int, popularity: dataSelected["popularity"] as! Double, poster_path: dataSelected["poster_path"] as! String, title: dataSelected["title"] as! String, overview: dataSelected["overview"] as! String, original_language: dataSelected["original_language"] as! String, vote_count: dataSelected["vote_count"] as! Double, release_date: dataSelected["release_date"] as! String, video: dataSelected["video"] as! Int)
            contentController.isWatchList = localStorage.isDataAxist(id:dataSelected["id"] as! Int, contentType: contentType)
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
        }else if contentType == 1 {
            contentController.TVShowSelected = TVShow(id: dataSelected["id"] as! Int, vote_average: dataSelected["vote_average"] as! Double, genre_ids: dataSelected["genre_ids"] as! [Int], original_name: dataSelected["original_name"] as! String, origin_country: dataSelected["origin_country"] as! [String], original_language: dataSelected["original_language"] as! String, backdrop_path: dataSelected["backdrop_path"] as? String == nil ? "none.jpg" : dataSelected["backdrop_path"] as! String, popularity: dataSelected["popularity"] as! Double, poster_path: dataSelected["poster_path"] is String ? dataSelected["poster_path"] as! String: "None" , title: dataSelected["name"] as! String, overview: dataSelected["overview"] as! String == "" ? "None": dataSelected["overview"] as! String , first_air_date: dataSelected["first_air_date"] as! String, vote_count: dataSelected["vote_count"] as! Int)
             contentController.isWatchList = localStorage.isDataAxist(id: dataSelected["id"] as! Int, contentType: contentType)
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
            
        }
    }
}
