//
//  SearchController.swift
//  TheMovieYours
//
//  Created by User on 2019/7/3.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import Alamofire
private let reuseIdentifier = "reuseContentCell"
class SearchController: UIViewController , UITextFieldDelegate {

    // MARK: - Properties
    var searchView: UIView!
    var btnView: UIView!
    var searchTextField: BaseTextField!
    var btnMovie:BaseButton!
    var btnTVShow: BaseButton!
    var typePage: Int = 0
    var movieList: [Movie]  = []
    var TVShowList: [TVShow]  = []
     var localStorage = LocalStorage()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isScrollEnabled =  true
        collectionView.backgroundColor = UIColor.init(hex: background_color).withAlphaComponent(0.5)
        return collectionView
    }()
    let indicatorView: UIView = {
        let viewbackground = UIView()
        viewbackground.backgroundColor = UIColor.init(hex: color_base_black).withAlphaComponent(0.5)
        viewbackground.translatesAutoresizingMaskIntoConstraints = false
        return viewbackground
    }()
    
    //MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.init(hex: color_base_white)

       configureNavigationBar()
        configureSearch()
        configureContentBtn()
        configureContent()
        configureIndicatorView()
    }
    
    //MARK: - Handles
    func configureNavigationBar(){
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: background_color)
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = ListBtn.Search.description
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "baseline_clear_white_36pt_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handlesDismiss))
    }
    
    @objc func handlesDismiss () {
        dismiss(animated: true, completion: nil)
    }
    
    func configureSearch() {
        searchView = UIView(frame: CGRect(x:0, y: (navigationController?.navigationBar.frame.height ?? 80) + 20, width: self.view.frame.width, height: 50))
        searchView.backgroundColor = UIColor.white
        view.addSubview(searchView)
        
        searchTextField = BaseTextField(frame: CGRect(x:5, y: 5, width: self.view.frame.width - 10, height: 40))
        searchTextField.addInset()
        searchTextField.updateDifferentClolor()
        searchTextField.updatePlaceholder(String: ListBtn.Search.description)
        searchView.addSubview(searchTextField)
        searchTextField.delegate = self
    }
    func configureContentBtn(){
        btnView = UIView(frame: CGRect(x:0, y:self.searchView.frame.origin.y + 50, width: self.view.frame.width, height: 50))
        btnView.backgroundColor = UIColor.white
        view.addSubview(btnView)
        
        let width: CGFloat = (self.view.frame.width - 20 ) / 2
        btnMovie = BaseButton(frame: CGRect(x: 5, y: 5, width: width, height: 40))
        btnMovie.tag = 0
        btnMovie.setTitle("Movie", for: .normal)
        btnMovie.updateButtonWithBackgroundColor()
        btnMovie.addTarget(self, action: #selector(selectionContent), for: .touchUpInside)
        btnView.addSubview(btnMovie)
        
        btnTVShow = BaseButton(frame: CGRect(x: width + 10, y: 5, width: width, height: 40))
        btnTVShow.tag = 1
        btnTVShow.setTitle("TV Show", for: .normal)
        btnTVShow.updateButtonWithBorder()
        btnTVShow.addTarget(self, action: #selector(selectionContent), for: .touchUpInside)
        btnView.addSubview(btnTVShow)
    }
    func configureContent() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ContentCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        view.addSubview(collectionView)
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: self.btnView.frame.origin.y + 50).isActive = true
    }
    func configureIndicatorView(){
        let viewIndicator = UIView(frame: CGRect(x: (self.view.frame.width / 2) - 50, y: (self.view.frame.height / 2) - 50, width: 100, height: 100))
        viewIndicator.layer.cornerRadius = 8;
        viewIndicator.clipsToBounds = true;
        viewIndicator.backgroundColor = UIColor.init(hex: color_base_white)
        viewIndicator.translatesAutoresizingMaskIntoConstraints = false
        let activityView = UIActivityIndicatorView(frame: CGRect(x:0, y: 0, width: 100, height: 70))
        activityView.style = .gray
        activityView.startAnimating()
        viewIndicator.addSubview(activityView)
        let labelIndicator = BaseLabel(frame: CGRect(x:0, y: 70, width: 100, height: 30))
        labelIndicator.textColor = .gray
        labelIndicator.titleDefultLabel()
        labelIndicator.textAlignment = .center
        labelIndicator.text = "Loading"
        viewIndicator.addSubview(labelIndicator)
        indicatorView.insertSubview(viewIndicator, at: 0)
       
        view.insertSubview(indicatorView, aboveSubview: indicatorView)
        indicatorView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        indicatorView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        indicatorView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        indicatorView.topAnchor.constraint(equalTo: view.topAnchor, constant: self.btnView.frame.origin.y + 50).isActive = true
        indicatorView.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        actionSearch()
        return false
    }
    func actionSearch() {
        var keywordTF:String = searchTextField.text!
        if  keywordTF.count != 0 {
            print("masukk")
            keywordTF = keywordTF.replacingOccurrences(of: " ", with: "%20")
        if self.typePage == 0 {
            self.indicatorView.isHidden = false
            self.movieList.removeAll()
            self.TVShowList.removeAll()
            updateContent()
            APIManager.init().getSearchMovieContent(typeContent: typePage, keyWord: keywordTF) { (result, err) in
                if !err {
                    self.movieList = result
                    self.updateContent()
                     self.indicatorView.isHidden = true
                }
            }
        } else if self.typePage == 1 {
             self.indicatorView.isHidden = false
            self.movieList.removeAll()
            self.TVShowList.removeAll()
            updateContent()
            APIManager.init().getSearchTVShowContent(typeContent: typePage, keyWord: keywordTF) { (result, err) in
                if !err {
                    self.TVShowList = result
                    self.updateContent()
                     self.indicatorView.isHidden = true
                }
            }
        }
        }
    
    }
    @IBAction func selectionContent(_ sender: Any) {
      
        if  (sender as AnyObject).tag == 0{
            self.typePage = 0
            btnMovie.updateButtonWithBackgroundColor()
            btnTVShow.updateButtonWithBorder()
            actionSearch()
            
        } else  if  (sender as AnyObject).tag == 1{
            self.typePage = 1
            btnTVShow.updateButtonWithBackgroundColor()
            btnMovie.updateButtonWithBorder()
           actionSearch()
        }
    }
    
    func updateContent(){
        self.collectionView.reloadData()
        let context = self.collectionView.collectionViewLayout.invalidationContext(forBoundsChange: self.collectionView.bounds)
        context.contentOffsetAdjustment = CGPoint.zero
        self.collectionView.collectionViewLayout.invalidateLayout(with: context)
        self.collectionView.layoutSubviews()
    }
}
extension SearchController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 10
        
        if self.typePage == 0{
            count = movieList.count
        } else if self.typePage == 1{
            count = TVShowList.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ContentCell
        cell.backgroundColor = .white
        cell.layer.cornerRadius = 4;
        cell.clipsToBounds = true;
        
        if self.typePage == 0{
            cell.titleLabel.text = self.movieList[indexPath.row].title as String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.movieList[indexPath.row].poster_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        } else if self.typePage == 1{
            cell.titleLabel.text = self.TVShowList[indexPath.row].title as String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.TVShowList[indexPath.row].poster_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell: CGFloat = (self.view.frame.width - 40) / 2
        return CGSize(width: widthCell, height: widthCell * 1.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print(indexPath.row)
        let contentController = ContentController()
        contentController.typePage = self.typePage
        if self.typePage == 0 {
            contentController.movieSelected = self.movieList[indexPath.row]
            print(self.movieList[indexPath.row].id)
            let isAxist : Bool = localStorage.isDataAxist(id: self.movieList[indexPath.row].id, contentType: self.typePage) as Bool
            contentController.isWatchList = isAxist
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
        } else if self.typePage == 1 {
            contentController.TVShowSelected =  self.TVShowList[indexPath.row]
            let isAxist : Bool = localStorage.isDataAxist(id: self.TVShowList[indexPath.row].id, contentType: self.typePage) as Bool
            contentController.isWatchList = isAxist
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
            
        }
    }
    
}
