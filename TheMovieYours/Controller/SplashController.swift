//
//  SplashController.swift
//  TheMovieYours
//
//  Created by User on 2019/7/2.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class SplashController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.init(hex: background_color)
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 25)
        label.textColor = UIColor.init(hex: color_base_white)
        label.textAlignment = .center
        let width:CGFloat = self.view.frame.width - 20
        let x:CGFloat = (self.view.frame.width / 2) - (width/2)
        let y:CGFloat = (self.view.frame.height / 2) - 30
        label.frame = CGRect(x: x, y: y, width: width, height: 60)
        view.addSubview(label)
        label.text = "The Movie Your"
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            if self.currentReachabilityStatus == .notReachable {
                self.alertConnection()
            } else {
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    label.frame = CGRect(x: x, y: y + 100, width: width, height: 60)
                }) { (_) in
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                        label.frame = CGRect(x: x, y: y - 50, width: width, height: 60)
                    }) { (_) in
                        let containerController = ContainerController()
                        self.present(containerController, animated: true, completion: nil)
                    }
                }
            }
        })
        

       
        // Do any additional setup after loading the view.
    }
    
    func alertConnection(){
        let alert = UIAlertController(title: "Connection", message: "There isn't internet connection", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(5.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            alert.dismiss(animated: true, completion: {() -> Void in
                exit(0)
            })
            
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
