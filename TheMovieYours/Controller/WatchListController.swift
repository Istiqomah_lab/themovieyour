//
//  WatchListController.swift
//  TheMovieYours
//
//  Created by User on 2019/7/2.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
private let reuseIdentifier = "reuseContentCell"
class WatchListController: UIViewController {

    // MARK: - Properties
    var movieList: [Movie]  = []
    var TVShowList: [TVShow]  = []
    var localStorage = LocalStorage()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isScrollEnabled =  true
        collectionView.backgroundColor = UIColor.init(hex: background_color).withAlphaComponent(0.5)
        return collectionView
    }()
    
    // MARK: -Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        convigureNavigationBar()
        configureContent()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getData()
    }
    // MARK: - Handler
    func convigureNavigationBar(){
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: background_color)
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = ListBtn.WatchList.description
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "baseline_clear_white_36pt_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handlesDismiss))
    }
    
    @objc func handlesDismiss () {
        dismiss(animated: true, completion: nil)
    }
    func getData(){
        localStorage.getMovieList { (result) in
            self.movieList = result
            self.collectionView.reloadData()
            let context = self.collectionView.collectionViewLayout.invalidationContext(forBoundsChange: self.collectionView.bounds)
            context.contentOffsetAdjustment = CGPoint.zero
            self.collectionView.collectionViewLayout.invalidateLayout(with: context)
            self.collectionView.layoutSubviews()
        }
        localStorage.getTVShowList { (result) in
            self.TVShowList = result
            self.collectionView.reloadData()
            let context = self.collectionView.collectionViewLayout.invalidationContext(forBoundsChange: self.collectionView.bounds)
            context.contentOffsetAdjustment = CGPoint.zero
            self.collectionView.collectionViewLayout.invalidateLayout(with: context)
            self.collectionView.layoutSubviews()
        }
    }
    func configureContent() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ContentCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        view.addSubview(collectionView)
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: (navigationController?.navigationBar.frame.height ?? 80)).isActive = true
    }


    

}
extension WatchListController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieList.count + self.TVShowList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ContentCell
        cell.backgroundColor = .white
        cell.layer.cornerRadius = 4;
        cell.clipsToBounds = true;
        if indexPath.row < self.movieList.count {
            cell.titleLabel.text = self.movieList[indexPath.row].title as String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.movieList[indexPath.row].poster_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        } else {
            cell.titleLabel.text = self.TVShowList[indexPath.row - self.movieList.count].title as String
            let url = URL(string: "https://image.tmdb.org/t/p/w500\(self.TVShowList[indexPath.row - self.movieList.count].poster_path as String)")
            Alamofire.request(url!).responseData { response in
                if let image = response.result.value {
                    cell.contentImage.image = UIImage(data: image)
                    
                }
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell: CGFloat = (self.view.frame.width - 40) / 2
        return CGSize(width: widthCell, height: widthCell * 1.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let contentController = ContentController()
        if indexPath.row < self.movieList.count {
            let index: Int = indexPath.row
            contentController.typePage = 0
            contentController.movieSelected = self.movieList[index]
            contentController.isWatchList = true
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
        } else {
             let index: Int = indexPath.row - self.movieList.count
            contentController.typePage = 1
            contentController.TVShowSelected = self.TVShowList[index]
            contentController.isWatchList = true
            present(UINavigationController(rootViewController: contentController), animated: true, completion: nil)
        }
    }

}
