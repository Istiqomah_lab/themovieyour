//
//  APIManager.swift
//  TheMovieYours
//
//  Created by User on 2019/6/24.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import Alamofire

struct Movie {
    var id: Int
    var vote_average: Double
    var genre_ids: [Int]
    var original_title: String
    var backdrop_path: String
    var adult: Int
    var popularity: Double
    var poster_path: String
    var title: String
    var overview: String
    var original_language: String
    var vote_count: Double
    var release_date: String
    var video: Int
 
}
struct TVShow {
    var id: Int
    var vote_average: Double
    var genre_ids: [Int]
    var original_name: String
    var origin_country: [String]
    var original_language: String
    var backdrop_path: String
    var popularity: Double
    var poster_path: String
    var title: String
    var overview: String
    var first_air_date: String
    var vote_count: Int
    
}
struct PopularPerson {
    var popularity: Double
    var id: Int
    var profile_path: String
    var title: String
    var adult: Int
    var known_for: NSArray!
}

struct PersonProfile {
    var id: Int
    var also_known_as: NSArray!
    var place_of_birth: String
    var birthday: String
    var biography: String
}
class APIManager: NSObject {
    func getMovieContent(typeAPI: String, page: String, complation: @escaping (_ result:[Movie], _ error:Bool) -> Void)   {
        getRequestAPICall(typeContent: 0, typeAPI: typeAPI, page: page) { (result, err) in
            
            var arrMovieList : [Movie]  = []
            if !err{
                let dicData = result 
                let arrData = dicData["results"] as! NSArray
                for list in arrData {
                    let dicList = list as! NSDictionary
//                    print(dicList.allKeys)
                    arrMovieList.append(Movie(id: dicList["id"] as! Int, vote_average: dicList["vote_average"] as! Double, genre_ids: dicList["genre_ids"] as! [Int], original_title: dicList["original_title"] as! String, backdrop_path: dicList["backdrop_path"] as? String == nil ? "none.jpg" : dicList["backdrop_path"] as! String, adult:  dicList["adult"] as! Int, popularity: dicList["popularity"] as! Double, poster_path: dicList["poster_path"] as? String == nil ? "none.jpg" : dicList["poster_path"] as! String, title: dicList["title"] as! String, overview: dicList["overview"] as! String, original_language: dicList["original_language"] as! String, vote_count: dicList["vote_count"] as! Double, release_date: dicList["release_date"] as! String, video: dicList["video"] as! Int))
                    
                }
                complation(arrMovieList,false)
            } else {
                complation(arrMovieList,true)
            }
        }
    }
    
    func getTVShowContent(typeAPI: String, page: String, complation: @escaping (_ result:[TVShow], _ error:Bool) -> Void)   {
        getRequestAPICall(typeContent: 1, typeAPI: typeAPI, page: page) { (result, err) in
            var arrTVShowList : [TVShow]  = []
            if !err {
                let dicData = result
                let arrData = dicData["results"] as! NSArray
                for list in arrData {
                    let dicList = list as! NSDictionary
                    arrTVShowList.append(TVShow(id: dicList["id"] as! Int, vote_average: dicList["vote_average"] as! Double, genre_ids: dicList["genre_ids"] as! [Int], original_name: dicList["original_name"] as! String, origin_country: dicList["origin_country"] as! [String], original_language: dicList["original_language"] as! String, backdrop_path: dicList["backdrop_path"] as? String == nil ? "none.jpg" : dicList["backdrop_path"] as! String, popularity: dicList["popularity"] as! Double, poster_path: dicList["poster_path"] as? String == nil ? "none.jpg" : dicList["poster_path"] as! String, title: dicList["name"] as! String, overview: dicList["overview"] as! String == "" ? "None": dicList["overview"] as! String , first_air_date: dicList["first_air_date"] as! String, vote_count: dicList["vote_count"] as! Int))
                }
                
                complation(arrTVShowList,false)
            } else {
                complation(arrTVShowList,true)
            }
        }
        
    }

    func getPopularPersonContent(typeAPI: String, page: String, complation: @escaping (_ result:[PopularPerson], _ error:Bool) -> Void)   {
        getRequestAPICall(typeContent: 3, typeAPI: typeAPI, page: page) { (result, err) in
            var arrPopPersonList : [PopularPerson]  = []
            if !err {
                let dicData = result
                let arrData = dicData["results"] as! NSArray
                for list in arrData {
                    let dicList = list as! NSDictionary
                    print(dicList.allKeys)
                    arrPopPersonList.append(PopularPerson(popularity: dicList["popularity"] as! Double, id: dicList["id"] as! Int, profile_path: dicList["profile_path"] as! String, title: dicList["name"] as!String, adult: dicList["adult"] as!Int, known_for:dicList["known_for"] as? NSArray))
                    
                }
                complation(arrPopPersonList,false)
            } else {
                complation(arrPopPersonList,true)
            }
        }
    }
    func getPersonBiography(id: Int, complation: @escaping (_ result:PersonProfile, _ error:Bool) -> Void)   {
        getRequestAPICall(typeContent: 2, typeAPI: "/person/\(id)", page: "1") { (result, err) in
            var arrPersonBioList :PersonProfile!
            if !err {
                print(result)
                let dicData = result
                let data = dicData["popularity"] as! Double
                arrPersonBioList = PersonProfile(id: dicData["id"] as! Int, also_known_as: dicData["also_known_as"] as! NSArray?, place_of_birth: dicData["place_of_birth"] as? String == nil ? "-":dicData["place_of_birth"] as! String, birthday: dicData["birthday"] as? String == nil ? "-":dicData["birthday"] as! String, biography: dicData["biography"] as! String)
                complation(arrPersonBioList,false)
            } else {
                complation(arrPersonBioList,true)
            }
        }
    }

    func getRequestAPICall(typeContent:Int, typeAPI: String, page: String, complation: @escaping (_ result:NSDictionary, _ error:Bool) -> Void)  {
        
        let Url: String = "https://api.themoviedb.org/3\(typeAPI)?api_key=9196d1fd50e6da355886ead7ebcc1cc6&language=en-US"
        
        Alamofire.request(Url, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in

                let dataDic = NSDictionary()
                switch response.result{
                    case .success(let data):
                        complation(data as! NSDictionary, false)
                   
                    case .failure(let error):
                        print(error.localizedDescription)
                        complation(dataDic ,true)
                    
                
                }
        }
    }
    
    func getSearchMovieContent(typeContent:Int,keyWord: String, complation: @escaping (_ result:[Movie], _ error:Bool) -> Void)   {
        getRequestSearchAPICall(typeContent: typeContent, keyWord: keyWord) { (result, err) in
            
            var arrMovieList : [Movie]  = []
            if !err{
                let dicData = result
                let arrData = dicData["results"] as! NSArray
                for list in arrData {
                    let dicList = list as! NSDictionary
                    print(dicList)
                    arrMovieList.append(Movie(id: dicList["id"] as! Int, vote_average: dicList["vote_average"] as! Double, genre_ids: dicList["genre_ids"] as! [Int], original_title: dicList["original_title"] as! String, backdrop_path: dicList["backdrop_path"] as? String == nil ? "none.jpg" : dicList["backdrop_path"] as! String, adult:  dicList["adult"] as! Int, popularity: dicList["popularity"] as! Double, poster_path: dicList["poster_path"] as? String == nil ? "none.jpg" : dicList["poster_path"] as! String, title: dicList["title"] as! String, overview: dicList["overview"] as! String, original_language: dicList["original_language"] as! String, vote_count: dicList["vote_count"] as! Double, release_date: dicList["release_date"] as! String, video: dicList["video"] as! Int))
                    
                }
                complation(arrMovieList,false)
            } else {
                complation(arrMovieList,true)
            }
        }
    }
    
    func getSearchTVShowContent(typeContent:Int,keyWord: String, complation: @escaping (_ result:[TVShow], _ error:Bool) -> Void)   {
        getRequestSearchAPICall(typeContent: typeContent, keyWord: keyWord) { (result, err) in
            var arrTVShowList : [TVShow]  = []
            if !err {
                let dicData = result
                let arrData = dicData["results"] as! NSArray
                for list in arrData {
                    let dicList = list as! NSDictionary
                    print(dicList)
                    arrTVShowList.append(TVShow(id: dicList["id"] as! Int, vote_average: dicList["vote_average"] as! Double, genre_ids: dicList["genre_ids"] as! [Int], original_name: dicList["original_name"] as! String, origin_country: dicList["origin_country"] as! [String], original_language: dicList["original_language"] as! String, backdrop_path: dicList["backdrop_path"] as? String == nil ? "none.jpg" : dicList["backdrop_path"] as! String, popularity: dicList["popularity"] as! Double, poster_path: dicList["poster_path"] as? String == nil ? "none.jpg" : dicList["poster_path"] as! String, title: dicList["name"] as! String, overview: dicList["overview"] as! String == "" ? "None": dicList["overview"] as! String , first_air_date: dicList["first_air_date"] as! String, vote_count: dicList["vote_count"] as! Int))
                }
                
                complation(arrTVShowList,false)
            } else {
                complation(arrTVShowList,true)
            }
        }
        
    }
    func getRequestSearchAPICall(typeContent:Int,keyWord: String, complation: @escaping (_ result:NSDictionary, _ error:Bool) -> Void)  {
        
        let Url: String = "https://api.themoviedb.org/3/search/\(typeContent == 0 ? "movie": "tv")?api_key=9196d1fd50e6da355886ead7ebcc1cc6&language=en-US&query=\(keyWord)&page=1&include_adult=false"
        
        Alamofire.request(Url, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                let dataDic = NSDictionary()
                switch response.result{
                case .success(let data):
                    complation(data as! NSDictionary, false)
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    complation(dataDic ,true)
                    
                    
                }
        }
    }


}
