//
//  LocalStorage.swift
//  TheMovieYours
//
//  Created by User on 2019/7/2.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation
import RealmSwift
import CloudKit

class LocalStorage: NSObject {
    var realm: Realm!
    
    //MARK: Array object
    var objectsArrayMovieStorage: Results<MovieStorage> {
        get{
            return realm.objects(MovieStorage.self)
        }
    }
    var objectsArrayTVStorage: Results<TVStorage> {
        get{
            return realm.objects(TVStorage.self)
        }
    }
    
    override init() {
        realm = try! Realm()
        
    }
    
    //MARK: - MovieList
    func saveMovieWatchList(movie: Movie) {
        let item =  MovieStorage()
        item.id = movie.id
        item.vote_average = movie.vote_average
        let arrayGenres = movie.genre_ids
        for list in arrayGenres {
            item.genre_ids.append(list)
        }
        item.original_title = movie.original_title
        item.backdrop_path = movie.backdrop_path
        item.adult = movie.adult
        item.popularity = movie.popularity
        item.poster_path = movie.poster_path
        item.title = movie.title
        item.overview = movie.overview
        item.original_language = movie.original_language
        item.vote_count = movie.vote_count
        item.release_date = movie.release_date
        item.video = movie.video
        try! self.realm.write {
            self.realm.add(item)
            print("success put daily data in local storage")
            
        }
    }
    func showMovieList(){
        for data in objectsArrayMovieStorage {
            print(data)
        }
    }
    func getMovieList( complation: @escaping (_ result:[Movie]) -> Void)   {
        var arrMovieList : [Movie]  = []
        for data in objectsArrayMovieStorage {
            let arrayGenres = data.genre_ids
            var genres:[Int] = []
            for list in arrayGenres {
                genres.append(list)
            }
            arrMovieList.append(Movie(id: data.id , vote_average: data.vote_average , genre_ids: genres , original_title: data.original_title , backdrop_path: data.backdrop_path , adult:  data.adult , popularity: data.popularity , poster_path: data.poster_path , title: data.title , overview: data.overview , original_language: data.original_language , vote_count: data.vote_count , release_date: data.release_date, video: data.video))
        }
        complation(arrMovieList)
    }
    func isDataAxist(id : Int, contentType:Int) -> Bool {
        let predicate = NSPredicate(format: "id == \(id)")
        var axist: Bool = false
        if contentType == 0{
            let filterMovie =  objectsArrayMovieStorage.filter(predicate)
            axist =  filterMovie.count != 0 ? true : false
        } else if contentType == 1{
            let filterTvShow =  objectsArrayTVStorage.filter(predicate)
            axist =  filterTvShow.count != 0 ? true : false
        }
        
       return axist
    }
    func  delateDataById(id : Int, contentType:Int) {
        let predicate = NSPredicate(format: "id == \(id)")
        if contentType == 0{
            let filterMovie =  objectsArrayMovieStorage.filter(predicate)
            try! realm.write {
                realm.delete(filterMovie)
                print("success delete daily data in local storage")
            }
        } else if contentType == 1{
            let filterTvShow =  objectsArrayTVStorage.filter(predicate)
            try! realm.write {
                realm.delete(filterTvShow)
                print("success delete daily data in local storage")
            }
        }
    }
    
    //MARK: - TVShowList
    func saveTVShowWatchList(tvShow: TVShow) {
        let item =  TVStorage()
        item.id = tvShow.id
        item.vote_average = tvShow.vote_average
        let arrayGenres = tvShow.genre_ids
        for list in arrayGenres {
            item.genre_ids.append(list)
        }
        item.original_name = tvShow.original_name
        let arrayOriCountry = tvShow.origin_country
        for list in arrayOriCountry {
            item.origin_country.append(list)
        }
        item.backdrop_path = tvShow.backdrop_path
        item.popularity = tvShow.popularity
        item.poster_path = tvShow.poster_path
        item.title = tvShow.title
        item.overview = tvShow.overview
        item.original_language = tvShow.original_language
        item.vote_count = tvShow.vote_count
        item.first_air_date = tvShow.first_air_date
        try! self.realm.write {
            self.realm.add(item)
            print("success put daily data in local storage")
            
        }
    }
    func showTVShowList(){
        for data in objectsArrayTVStorage {
            print(data)
        }
    }
    func getTVShowList( complation: @escaping (_ result:[TVShow]) -> Void)   {
        var arrTVShowList : [TVShow]  = []
        for data in objectsArrayTVStorage {
            let arrayGenres = data.genre_ids
            var genres:[Int] = []
            for list in arrayGenres {
                genres.append(list)
            }
            let arrayOriCountry = data.origin_country
            var originalCountry:[String] = []
            for list in arrayOriCountry {
                originalCountry.append(list)
            }
            arrTVShowList.append(TVShow(id: data.id , vote_average: data.vote_average, genre_ids: genres, original_name: data.original_name, origin_country: originalCountry, original_language: data.original_language, backdrop_path: data.backdrop_path, popularity: data.popularity, poster_path: data.poster_path, title: data.title, overview: data.overview, first_air_date: data.first_air_date, vote_count: data.vote_count))
        }
        complation(arrTVShowList)
    }
    
}
