//
//  ObjectRealM.swift
//  TheMovieYours
//
//  Created by User on 2019/7/2.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation
import RealmSwift

class MovieStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var vote_average: Double = 0.0
    var genre_ids = List<Int>()
    @objc dynamic var original_title: String = ""
    @objc dynamic var backdrop_path: String = ""
    @objc dynamic var adult: Int = 0
    @objc dynamic var popularity: Double = 0.0
    @objc dynamic var poster_path: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var original_language: String = ""
    @objc dynamic var vote_count: Double = 0.0
    @objc dynamic var release_date: String = ""
    @objc dynamic var video: Int = 0
    
}
class TVStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var vote_average: Double = 0.0
    var genre_ids = List<Int>()
    @objc dynamic var original_name: String = ""
    var origin_country = List<String>()
    @objc dynamic var original_language: String = ""
    @objc dynamic var backdrop_path: String = ""
    @objc dynamic var popularity: Double = 0.0
    @objc dynamic var poster_path: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic  var first_air_date: String = ""
    @objc dynamic var vote_count: Int = 0
}
