//
//  MenuList.swift
//  TheMovieYours
//
//  Created by User on 2019/6/28.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
enum MenuList:Int,CustomStringConvertible {
    case TopRatedMovies
    case UpcomingMovies
    case NowPlayingMovies
    case PopularMovies
    case TopRatedTVShows
    case PopularTVShows
    case OnTheAirTVShows
    case AiringTodayTVShows
    case PopularPeople
    
    
    var description: String{
        switch self {
        case .TopRatedMovies: return "Top Rated Movies"
        case .UpcomingMovies: return "Upcoming Movies"
        case .NowPlayingMovies: return "Now Playing Movies"
        case .PopularMovies: return "Popular Movies"
        case .TopRatedTVShows: return "Top Rated TV Shows"
        case .PopularTVShows: return "Popular TV Shows"
        case .OnTheAirTVShows: return "On The Air TV Shows"
        case .AiringTodayTVShows: return "Airing Today TV Shows"
        case .PopularPeople: return "Popular People"
        }
    }
    
    var APIDescription: String{
        switch self {
        case .TopRatedMovies: return "/movie/top_rated"
        case .UpcomingMovies: return "/movie/upcoming"
        case .NowPlayingMovies: return "/movie/now_playing"
        case .PopularMovies: return "/movie/popular"
        case .TopRatedTVShows: return "/tv/top_rated"
        case .PopularTVShows: return "/tv/popular"
        case .OnTheAirTVShows: return "/tv/on_the_air"
        case .AiringTodayTVShows: return "/tv/airing_today"
        case .PopularPeople: return "/person/popular"
        }
    }
    var typeContent: Int{
        switch self {
        case .TopRatedMovies: return 0
        case .UpcomingMovies: return 0
        case .NowPlayingMovies: return 0
        case .PopularMovies: return 0
        case .TopRatedTVShows: return 1
        case .PopularTVShows: return 1
        case .OnTheAirTVShows: return 1
        case .AiringTodayTVShows: return 1
        case .PopularPeople: return 2
        }
    }
    static var count: Int { return MenuList.PopularPeople.rawValue + 1}
}
enum genresMovie:Int,CustomStringConvertible {
    
    case Action = 28
    case Adventure = 12
    case Animation = 16
    case Comedy = 35
    case Crime = 80
    case Documentary = 99
    case Drama = 18
    case Family = 10751
    case Fantasy = 14
    case History = 36
    case Horror = 27
    case Music = 10402
    case Mystery = 9648
    case Romance = 10749
    case ScienceFiction = 878
    case TVMovie = 10770
    case Thriller = 53
    case War = 10752
    case Western = 37
    
    
    var description: String{
        switch self {
        case .Action: return "Action"
        case .Adventure: return"Adventure"
        case .Animation:return "Animation"
        case .Comedy: return "Comedy"
        case .Crime: return"Crime"
        case .Documentary: return"Documentary"
        case .Drama: return "Drama"
        case .Family:return "Family"
        case .Fantasy:return "Fantasy"
        case .History:return "History"
        case .Horror:return "Horror"
        case .Music:return "Music"
        case .Mystery:return "Mystery"
        case .Romance:return "Romance"
        case .ScienceFiction:return "Science Fiction"
        case .TVMovie:return "TV Movie"
        case .Thriller:return "Thriller"
        case .War:return "War"
        case .Western:return "Western"
        
        }
    }
}
enum genresTVShow:Int,CustomStringConvertible {
    
    case ActionAdventure = 10759
    case Adventure = 12
    case Animation = 16
    case Comedy = 35
    case Crime = 80
    case Documentary = 99
    case Drama = 18
    case Family = 10751
    case Fantasy = 14
    case History = 36
    case Horror = 27
    case Music = 10402
    case Mystery = 9648
    case Romance = 10749
    case ScienceFiction = 878
    case TVMovie = 10770
    case Thriller = 53
    case War = 10752
    case Western = 37
    case Kids = 10762
    case News = 10763
    case Reality = 10764
    case SciFiFantasy = 10765
    case Soap = 10766
    case Talk = 10767
    case WarPolitics = 10768
    
    var description: String{
        switch self {
        case .ActionAdventure: return "Action & Adventure"
        case .Adventure: return"Adventure"
        case .Animation: return "Animation"
        case .Comedy: return "Comedy"
        case .Crime: return"Crime"
        case .Documentary: return"Documentary"
        case .Drama: return "Drama"
        case .Family:return "Family"
        case .Fantasy:return "Fantasy"
        case .History:return "History"
        case .Horror:return "Horror"
        case .Music:return "Music"
        case .Mystery:return "Mystery"
        case .Romance:return "Romance"
        case .ScienceFiction:return "Science Fiction"
        case .TVMovie:return "TVMovie"
        case .Thriller:return "Thriller"
        case .War:return "War"
        case .Western:return "Western"
        case .Kids:return "Kids"
        case .News:return "News"
        case .Reality:return "Reality"
        case .SciFiFantasy:return "Sci-Fi & Fantasy"
        case .Soap:return "Soap"
        case .Talk:return "Talk"
        case .WarPolitics:return "War & Politics"
        }
    }
}
enum ListLableMovie:Int,CustomStringConvertible {
    
    case OriginalTitle
    case ReleaseDate
    case OriginalLanguage
    case Genres
    case Rating
    case Sinopsis
    var description: String{
        switch self {
        case .OriginalTitle: return "Original Title"
        case .ReleaseDate:return "Release Date"
        case .OriginalLanguage:return "Original Language"
        case .Genres:return "Genres"
        case .Rating:return "Rating"
        case .Sinopsis:return "Synopsis"
        }
    }
}
enum ListLableTVShow:Int,CustomStringConvertible {
    
    case OriginalName
    case FirstAirDate
    case OriginalLanguage
    case Genres
    case Rating
    case Overview
    var description: String{
        switch self {
        case .OriginalName: return "Original Name"
        case .FirstAirDate:return "First Air Date"
        case .OriginalLanguage:return "Original Language"
        case .Genres:return "Genres"
        case .Rating:return "Rating"
        case .Overview:return "Overview"
        }
    }
}

enum ListLablePerson:Int,CustomStringConvertible {
    
    case Name
    case PlaceofBirth
    case Birthday
    case Biography
    var description: String{
        switch self {
        case .Name: return "Name"
        case .PlaceofBirth:return "Place of Birth"
        case .Birthday:return "Birthday"
        case .Biography:return "Biography"
        }
    }
}
enum ListBtn:Int,CustomStringConvertible {
    
    case WatchList
    case Search
    var description: String{
        switch self {
        case .WatchList: return "Watch List"
        case .Search:return "Search"

        }
    }
}

