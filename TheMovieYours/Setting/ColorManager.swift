//
//  ColorManager.swift
//  TheMovieYours
//
//  Created by User on 2019/6/28.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

//MARK: Base Button color
let color_btn_red:String = "cc0000"
let color_btn_blue:String = "007AFF"
let color_btn_gray:String = "7F7F7F"

let color_text_default:String = "0x00005a"
//Base Color
let color_base_white = "FFFFFF"
let color_base_black = "000000"
let color_base_yellow = "FFFF00"
let hex_color_base_white = "0xFFFFFF"

//let background_color = "0x05C99B"
let background_color = "0x00005a"
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
