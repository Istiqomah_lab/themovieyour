//
//  Protocol.swift
//  TheMovieYours
//
//  Created by User on 2019/6/28.
//  Copyright © 2019 User. All rights reserved.
//

protocol HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuList?)
    func handleWatchList()
    
}

