//
//  ContentCell.swift
//  TheMovieYours
//
//  Created by User on 2019/6/29.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class ContentCell: UICollectionViewCell {
    
    // MARK: - Properties
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.backgroundColor = UIColor.init(hex: color_base_black).withAlphaComponent(0.5)
        label.textAlignment = .center
        label.textColor = UIColor.init(hex: color_base_white)
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 2
        label.text = "sample text"
        return label
    }()
    let contentImage : UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named:"video-player")
        return image
    }()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
        
        addSubview(contentImage)
        contentImage.translatesAutoresizingMaskIntoConstraints =  false
        contentImage.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentImage.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        contentImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentImage.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        contentImage.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints =  false
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 70).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
