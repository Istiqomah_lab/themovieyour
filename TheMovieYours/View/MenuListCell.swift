//
//  MenuListCell.swift
//  TheMovieYours
//
//  Created by User on 2019/6/28.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class MenuListCell: UITableViewCell {

    //MARK: - Properties
    let descriotionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.init(hex: color_base_white)
        label.font = UIFont.systemFont(ofSize: 20)
        label.text = "sample text"
        return label
    }()
    
    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        addSubview(descriotionLabel)
        descriotionLabel.translatesAutoresizingMaskIntoConstraints =  false
        descriotionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        descriotionLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
