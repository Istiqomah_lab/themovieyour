//
//  TheMovieYoursTests.swift
//  TheMovieYoursTests
//
//  Created by User on 2019/6/24.
//  Copyright © 2019 User. All rights reserved.
//

import XCTest
import Alamofire
@testable import TheMovieYours
class TheMovieYoursTests: XCTestCase {
    func testValidMenuApp() {
        let firstMenu = MenuList(rawValue: 0)?.description
        XCTAssertEqual("Top Rated Movies", firstMenu)
        let secondMenu = MenuList(rawValue: 1)?.description
        XCTAssertEqual("Upcoming Movies", secondMenu)
        let thirdMenu = MenuList(rawValue: 2)?.description
        XCTAssertEqual("Now Playing Movies", thirdMenu)
        let fourthMenu = MenuList(rawValue: 3)?.description
        XCTAssertEqual("Popular Movies", fourthMenu)
        let fifthMenu = MenuList(rawValue: 4)?.description
        XCTAssertEqual("Top Rated TV Shows", fifthMenu)
        let sixthMenu = MenuList(rawValue: 5)?.description
        XCTAssertEqual("Popular TV Shows", sixthMenu)
        let seventhMenu = MenuList(rawValue: 6)?.description
        XCTAssertEqual("On The Air TV Shows", seventhMenu)
        let eighthMenu = MenuList(rawValue: 7)?.description
        XCTAssertEqual("Airing Today TV Shows", eighthMenu)
        let ninethMenu = MenuList(rawValue: 8)?.description
        XCTAssertEqual("Popular People", ninethMenu)
    }
    func testValidAPI(){
        let firstApi = MenuList(rawValue: 0)?.APIDescription
        XCTAssertEqual("/movie/top_rated", firstApi)
        let secondApi = MenuList(rawValue: 1)?.APIDescription
        XCTAssertEqual("/movie/upcoming", secondApi)
        let thirdApi = MenuList(rawValue: 2)?.APIDescription
        XCTAssertEqual("/movie/now_playing", thirdApi)
        let fourthApi = MenuList(rawValue: 3)?.APIDescription
        XCTAssertEqual("/movie/popular", fourthApi)
        let fifthApi = MenuList(rawValue: 4)?.APIDescription
        XCTAssertEqual("/tv/top_rated", fifthApi)
        let sixthApi = MenuList(rawValue: 5)?.APIDescription
        XCTAssertEqual("/tv/popular", sixthApi)
        let seventhApi = MenuList(rawValue: 6)?.APIDescription
        XCTAssertEqual("/tv/on_the_air", seventhApi)
        let eighthApi = MenuList(rawValue: 7)?.APIDescription
        XCTAssertEqual("/tv/airing_today", eighthApi)
        let ninethApi = MenuList(rawValue: 8)?.APIDescription
        XCTAssertEqual("/person/popular", ninethApi)
    }
    func testContentType() {
        XCTAssertEqual(0, MenuList(rawValue: 0)?.typeContent)
         XCTAssertEqual(0, MenuList(rawValue: 1)?.typeContent)
         XCTAssertEqual(0, MenuList(rawValue: 2)?.typeContent)
         XCTAssertEqual(0, MenuList(rawValue: 3)?.typeContent)
         XCTAssertEqual(1, MenuList(rawValue: 4)?.typeContent)
         XCTAssertEqual(1, MenuList(rawValue: 5)?.typeContent)
         XCTAssertEqual(1, MenuList(rawValue: 6)?.typeContent)
         XCTAssertEqual(1, MenuList(rawValue: 7)?.typeContent)
         XCTAssertEqual(2, MenuList(rawValue: 8)?.typeContent)
    }
    func testGenres()  {
        XCTAssertEqual("Action", genresMovie(rawValue: 28)?.description)
        XCTAssertEqual("Adventure", genresMovie(rawValue: 12)?.description)
        XCTAssertEqual("Animation", genresMovie(rawValue: 16)?.description)
        XCTAssertEqual("Comedy", genresMovie(rawValue: 35)?.description)
        XCTAssertEqual("Crime", genresMovie(rawValue: 80)?.description)
        XCTAssertEqual("Documentary", genresMovie(rawValue: 99)?.description)
        XCTAssertEqual("Drama", genresMovie(rawValue: 18)?.description)
        XCTAssertEqual("Family", genresMovie(rawValue: 10751)?.description)
        XCTAssertEqual("Fantasy", genresMovie(rawValue: 14)?.description)
        XCTAssertEqual("History", genresMovie(rawValue: 36)?.description)
        XCTAssertEqual("Horror", genresMovie(rawValue: 27)?.description)
        XCTAssertEqual("Music", genresMovie(rawValue: 10402)?.description)
        XCTAssertEqual("Mystery", genresMovie(rawValue: 9648)?.description)
        XCTAssertEqual("Romance", genresMovie(rawValue: 10749)?.description)
        XCTAssertEqual("Science Fiction", genresMovie(rawValue: 878)?.description)
        XCTAssertEqual("TV Movie", genresMovie(rawValue: 10770)?.description)
        XCTAssertEqual("Thriller", genresMovie(rawValue: 53)?.description)
        XCTAssertEqual("War", genresMovie(rawValue: 10752)?.description)
        XCTAssertEqual("Western", genresMovie(rawValue: 37)?.description)
        
        XCTAssertEqual("Action & Adventure", genresTVShow(rawValue: 10759)?.description)
        XCTAssertEqual("Animation", genresTVShow(rawValue: 16)?.description)
        XCTAssertEqual("Comedy", genresTVShow(rawValue: 35)?.description)
        XCTAssertEqual("Crime", genresTVShow(rawValue: 80)?.description)
        XCTAssertEqual("Documentary", genresTVShow(rawValue: 99)?.description)
        XCTAssertEqual("Drama", genresTVShow(rawValue: 18)?.description)
        XCTAssertEqual("Family", genresTVShow(rawValue: 10751)?.description)
        XCTAssertEqual("Kids", genresTVShow(rawValue: 10762)?.description)
        XCTAssertEqual("Mystery", genresTVShow(rawValue: 9648)?.description)
        XCTAssertEqual("News", genresTVShow(rawValue: 10763)?.description)
        XCTAssertEqual("Reality", genresTVShow(rawValue: 10764)?.description)
        XCTAssertEqual("Sci-Fi & Fantasy", genresTVShow(rawValue: 10765)?.description)
        XCTAssertEqual("Soap", genresTVShow(rawValue: 10766)?.description)
        XCTAssertEqual("Talk", genresTVShow(rawValue: 10767)?.description)
        XCTAssertEqual("War & Politics", genresTVShow(rawValue: 10768)?.description)
        XCTAssertEqual("Western", genresTVShow(rawValue: 37)?.description)
    }
    func testLabel() {
        XCTAssertEqual("Original Title", ListLableMovie(rawValue: 0)?.description)
        XCTAssertEqual("Release Date", ListLableMovie(rawValue: 1)?.description)
        XCTAssertEqual("Original Language", ListLableMovie(rawValue: 2)?.description)
        XCTAssertEqual("Genres", ListLableMovie(rawValue: 3)?.description)
        XCTAssertEqual("Rating", ListLableMovie(rawValue: 4)?.description)
        XCTAssertEqual("Sinopsis", ListLableMovie(rawValue: 5)?.description)
        
        XCTAssertEqual("Original Name", ListLableTVShow(rawValue: 0)?.description)
        XCTAssertEqual("First Air Date", ListLableTVShow(rawValue: 1)?.description)
        XCTAssertEqual("Original Language", ListLableTVShow(rawValue: 2)?.description)
        XCTAssertEqual("Genres", ListLableTVShow(rawValue: 3)?.description)
        XCTAssertEqual("Rating", ListLableTVShow(rawValue: 4)?.description)
        XCTAssertEqual("Overview", ListLableTVShow(rawValue: 5)?.description)
        
        XCTAssertEqual("Name", ListLablePerson(rawValue: 0)?.description)
        XCTAssertEqual("Place of Birth", ListLablePerson(rawValue: 1)?.description)
        XCTAssertEqual("Birthday", ListLablePerson(rawValue: 2)?.description)
        XCTAssertEqual("Biography", ListLablePerson(rawValue: 3)?.description)
        
    }
    
}
